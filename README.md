# Wizard for generating VSAAS Config

[![asciicast](https://asciinema.org/a/loIbgxiTEqe3nJN52ICbMXj2F.svg)](https://asciinema.org/a/loIbgxiTEqe3nJN52ICbMXj2F)

## Installation

(Requires [Poetry](https://python-poetry.org/) to be installed)

Install the dependencies:

```bash
poetry install
```

Or in Docker:

```docker
docker build .
```


## Running

Locally:

```bash
poetry shell
python vsaas_wizard/main.py
```

Docker:

```docker
docker run --rm -it vsaaswizard python /code/vsaas_wizard/main.py
```