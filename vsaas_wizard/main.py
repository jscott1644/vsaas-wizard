from yaml import load, dump

from questions.k8s import K8sQuestions
from questions.redis import RedisQuestions
from questions.postgres import PostgresQuestions
from questions.minio import MinIOQuestions

import click

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


@click.group()
def cli():
    pass

@cli.command(name='config')
def main():
    k = K8sQuestions()
    r = RedisQuestions()
    p = PostgresQuestions()
    m = MinIOQuestions()
    
    answers = {}
    answers.update(k.ask_questions())
    answers.update(r.ask_questions())
    answers.update(p.ask_questions())
    answers.update(m.ask_questions())


    # uppercase keys
    answers = {k.upper():v for k,v in answers.items()}

    y = {}

    y['apiVersion'] = 'v1'
    y['kind'] = 'ConfigMap'
    y['metadata'] = {
        'name': 'overcast-environment'
    }
    y['data'] = answers

    print(dump(y, Dumper=Dumper))


if __name__ == "__main__":
    cli()