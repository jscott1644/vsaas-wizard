import subprocess
import json
from PyInquirer import style_from_dict, Token, prompt
from questions.utils import IPsValidator, Inquirer


class K8sQuestions(Inquirer):
    def __init__(self):
        super().__init__()

    def ask_questions(self):
        answers = self._ask_k8s_setup()
        if answers['k8s_setup']:
            ans = self._ask_single_k8s()
            if not ans['k8s_single']:
                self._ask_k8s_new_nodes()
        else:
            self._ask_k8s_master()

        return self.answers

    def _ask_single_k8s(self):
        answers = prompt({
            'type': 'confirm',
            'name': 'k8s_single',
            'message': 'Should Kubernetes be installed on a single node (minikube)?',
            'default': False,
        })

        return self.update(answers)

    def _ask_k8s_setup(self):
        answers = prompt({
            'type': 'confirm',
            'name': 'k8s_setup',
            'message': 'Do you wish to confire a new Kubernetes cluster?',
            'default': False,
        })

        return self.update(answers)

    def _ask_k8s_new_nodes(self):
        answers = prompt([
            {
                'type': 'input',
                'name': 'k8s_master',
                'message': 'Please provide the IP address(es) of the master server:',
                'validate': IPsValidator
            },
            {
                'type': 'password',
                'name': 'k8s_master_pass',
                'message': 'Please provide the password for the master server:'
            },
            {
                'type': 'input',
                'name': 'k8s_nodes',
                'message': 'Please provide the IP addresses of the nodes:',
                'validate': IPsValidator
            },
            {
                'type': 'password',
                'name': 'k8s_node_pass',
                'message': 'Please provide the password for the node server(s):'
            }
        ])

        return self.update(answers)

    def _ask_k8s_master(self):
        cmd = 'kubectl config view --flatten=true -o json'.split(' ')

        output = subprocess.run(cmd, check=True, stdout=subprocess.PIPE)
        if output.returncode == 0:
            json_out = output.stdout
            kconfig = json.loads(json_out)
            current_context = kconfig['current-context']

            available_contexts = []
            for context in kconfig['contexts']:
                available_contexts.append(context['name'])

            # move current config to the start
            available_contexts.insert(0, available_contexts.pop(available_contexts.index(current_context)))

            answers = prompt([{
                'type': 'list',
                'name': 'kubectl_context',
                'message': 'Select kubectl context to use:',
                'choices': available_contexts
            }])

            answers['k8s_server'] = next(x['cluster']['server'] for x in kconfig['clusters'] if x['name'] == answers['kubectl_context'])
            self.update(answers)

        return self.answers