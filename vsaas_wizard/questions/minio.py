import json
from PyInquirer import style_from_dict, Token, prompt
from questions.utils import IPValidator, Inquirer


class MinIOQuestions(Inquirer):
    def __init__(self):
        super().__init__()

    def ask_questions(self):
        answers = self._ask_minio_setup()

        if answers['minio_setup']:
            ans = self._ask_minio_k8s()
            if not ans:
                self._ask_minio_host()
        else:
            ans = self._ask_storage_type()
            if ans['storage_media_type'] == 'minio':
                self._ask_minio_host()
        return self.answers

    def _ask_minio_setup(self):
        answers = prompt([{
            'type': 'confirm',
            'name': 'minio_setup',
            'message': 'Should Object Storage be installed?'
        }])

        self.answers.update(answers)
        return self.answers
    
    def _ask_minio_host(self):
        answers = prompt([
            {
                'type': 'input',
                'name': 'minio_endpoint',
                'message': 'What is the MinIO server\'s IP?',
                'validate': IPValidator
            },
            {
                'type': 'input',
                'name': 'minio_access_key',
                'message': 'Minio Access Key:',
            },
            {
                'type': 'input',
                'name': 'minio_secret_key',
                'message': 'Minio Secret Key'
            }
        ])

        self.answers.update(answers)
        return self.answers
    
    def _ask_minio_k8s(self):
        answers = prompt([{
            'type': 'confirm',
            'name': 'minio_k8s',
            'message': 'Should MinIO be installed into Kubernetes?'
        }])

        self.answers.update(answers)
        return self.answers

    def _ask_storage_type(self):
        answers = prompt([{
            'type': 'list',
            'name': 'storage_media_type',
            'message': 'Select storage media type:',
            'choices': [
                'minio',
                's3'
            ]
        }])

        self.answers.update(answers)
        return self.answers