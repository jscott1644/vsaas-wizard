import json
from PyInquirer import style_from_dict, Token, prompt
from questions.utils import IPValidator, Inquirer


class PostgresQuestions(Inquirer):
    def __init__(self):
        super().__init__()

    def ask_questions(self):
        answers = self._ask_postgres_setup()

        if answers['postgres_setup']:
            ans = self._ask_postgres_k8s()
            if not ans:
                self._ask_postgres_host()
        else:
            self._ask_postgres_host()
        return self.answers

    def _ask_postgres_setup(self):
        answers = prompt([{
            'type': 'confirm',
            'name': 'postgres_setup',
            'message': 'Should Postgres be installed?'
        }])

        return self.update(answers)
    
    def _ask_postgres_host(self):
        answers = prompt([
            {
                'type': 'input',
                'name': 'overcast_db_service_host',
                'message': 'What is the Postgres server\'s IP?',
                'validate': IPValidator
            },
            {
                'type': 'input',
                'name': 'overcast_db_service_port',
                'message': 'What is the Postgres server\'s Port?',
                'default': '5432'
            }
        ])

        return self.update(answers)
    
    def _ask_postgres_k8s(self):
        answers = prompt([{
            'type': 'confirm',
            'name': 'postgres_k8s',
            'message': 'Should Postgres be installed into Kubernetes?'
        }])

        return self.update(answers)