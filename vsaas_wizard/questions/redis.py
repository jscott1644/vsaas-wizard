import json
from PyInquirer import style_from_dict, Token, prompt
from questions.utils import IPValidator, Inquirer


class RedisQuestions(Inquirer):
    def __init__(self):
        super().__init__()

    def ask_questions(self):
        answers = self._ask_redis_setup()

        if answers['redis_setup']:
            ans = self._ask_redis_k8s()
            if not ans:
                self._ask_redis_host()
        else:
            self._ask_redis_host()
        return self.answers

    def _ask_redis_setup(self):
        answers = prompt([{
            'type': 'confirm',
            'name': 'redis_setup',
            'message': 'Should Redis be installed?'
        }])

        return self.update(answers)
    
    def _ask_redis_host(self):
        answers = prompt([
            {
                'type': 'input',
                'name': 'overcast_redis_service_host',
                'message': 'What is the Redis server\'s IP?',
                'validate': IPValidator
            },
            {
                'type': 'input',
                'name': 'overcast_redis_service_port',
                'message': 'What is the Redis server\'s Port?',
                'default': '6379'
            }
        ])

        return self.update(answers)
    
    def _ask_redis_k8s(self):
        answers = prompt([{
            'type': 'confirm',
            'name': 'redis_k8s',
            'message': 'Should Redis be installed into Kubernetes?'
        }])

        return self.update(answers)