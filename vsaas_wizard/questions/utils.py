import regex
from PyInquirer import Validator, ValidationError


class IPsValidator(Validator):
    """Check multiple IPs"""
    def validate(self, document):
        ips = document.text.split(',')
        for ip in ips:
            ok = regex.match('^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$', ip)
            if not ok: 
                raise ValidationError(
                    message='Please enter a valid IP address',
                    cursor_position=len(document.text)
                )

class IPValidator(Validator):
    def validate(self, document):
        ok = regex.match('^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$', document.text)
        if not ok: 
            raise ValidationError(
                message='Please enter a valid IP address',
                cursor_position=len(document.text)
            )
        
class Inquirer():
    def __init__(self):
        super().__init__()

        self.answers = {}
    
    def update(self, answers):
        self.answers.update(answers)
        return self.answers
    
    def get_answers(self):
        return self.answers
    
    def ask_questions(self):
        pass